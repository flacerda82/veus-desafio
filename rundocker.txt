***************************
MYSQL DOCKER
***************************
docker run --name my-mysql -v c:/data-mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=minhasenha -p 3306:3306 -d mysql:5.7.22

 sudo docker run --name my-mysql -v /home/felipe/dev/data-mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=minhasenha -p 3306:3306 -d mysql:5.7.22

***************************
RUN BACKOFFICE
***************************
sudo docker run -dit -p 9000:80 -v /home/felipe/dev/veus/app:/var/www/html --link my-mysql --name veus veus

sudo docker run -dit -p 9000:80 -v /home/felipe/dev/veus/app:/var/www/html --name veus veus

***************************
Limpar Containers - Windows
***************************
FOR /f "tokens=*" %i IN ('docker ps -a -q') DO docker rm -f %i

***************************
Limpar Images - Windows
***************************
FOR /f "tokens=*" %i IN ('docker images --format "{{.ID}}"') DO docker rmi -f %i

***************************
Limpar Containers - Linux
***************************
sudo docker rm $(sudo docker ps -a -q) -f

--remove-orphans

***************************
Limpar Images - Linux
***************************
sudo docker rmi $(sudo docker images -q)

sudo docker stop $(sudo docker ps -q)


chown www-data:www-data /var/www/html/storage -R